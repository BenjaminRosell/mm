"use strict";
////////////////////////////////////////////////////////
///////////////dropdown//////////////
////////////////////////////////////////////////////////
jQuery(function($){
    $(".dropdown").hover(            
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            });
    });
    

////////////////////////////////////////////////////////
///////////////on scroll animation effects//////////////
////////////////////////////////////////////////////////


new WOW().init();

////////////////////////////////////////////////////////
///////////////Smooth scroll for top navbar/////////////
////////////////////////////////////////////////////////


smoothScroll.init({
			speed: 1000,
			easing: 'easeInOutCubic',
			offset: 85,
			updateURL: false,
			callbackBefore: function ( toggle, anchor ) {},
			callbackAfter: function ( toggle, anchor ) {}
});

////////////////////////////////////////////////////////
///////////////Smooth scroll for top navbar/////////////
////////////////////////////////////////////////////////
// On document ready:
jQuery(document).ready(function() {
//ISOTOPE FUNCTION - FILTER PORTFOLIO FUNCTION
	var $portfolio;
	var $portfolio_selectors;
	jQuery(window).load(function($){
		$portfolio = jQuery('.portfolio-items');
		$portfolio.isotope({
			itemSelector : 'li',
			layoutMode : 'fitRows'
		});
		$portfolio_selectors = jQuery('.portfolio-filter >li>a');
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			jQuery(this).addClass('active');
			var selector = jQuery(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
	});
});
////////////////////////////////////////////////////////
///////////////Parallax effects/////////////////////////
////////////////////////////////////////////////////////


jQuery('div.bgParallax').each(function($){
	var $obj = $(this);

	$(window).scroll(function() {
		var yPos = -($(window).scrollTop() / $obj.data('speed')); 

		var bgpos = '50% '+ yPos + 'px';

		$obj.css('background-position', bgpos );
 
	}); 
});


////////////////////////////////////////////////////////
///////////////Image hovers effects and captions///////
////////////////////////////////////////////////////////


jQuery(window).load(function($){
  jQuery('.hcaption').hcaptions({effect: "fade"});
});


////////////////////////////////////////////////////////
///////////////prettyPhoto///////////////////////////
////////////////////////////////////////////////////////

jQuery("a.preview").prettyPhoto({
		social_tools: false
	});



////////////////////////////////////////////////////////
///////////////main slider ///////////////////////////
////////////////////////////////////////////////////////

	


////////////////////////////////////////////////////////
///////////////back to top ///////////////////////////
////////////////////////////////////////////////////////


jQuery(document).ready(function() {
				var offset = 220;
				var duration = 500;
				jQuery(window).scroll(function() {
					if (jQuery(this).scrollTop() > offset) {
						jQuery('.back-to-top').fadeIn(duration);
					} else {
						jQuery('.back-to-top').fadeOut(duration);
					}
				});
				
				jQuery('.back-to-top').click(function(event) {
					event.preventDefault();
					jQuery('html, body').animate({scrollTop: 0}, duration);
					return false;
			})
});

////////////////////////////////////////////////////////
///////////////gallery owl-carousel ///////////////////////////
////////////////////////////////////////////////////////

	
////////////////////////////////////////////////////////
///////////////preloader ///////////////////////////
////////////////////////////////////////////////////////

jQuery(window).load(function($) { // makes sure the whole site is loaded
			 jQuery('#status').fadeOut(); // will first fade out the loading animation
			 jQuery('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
			 jQuery('body').delay(350).css({'overflow':'visible'});
	});
	
////////////////////////////////////////////////////////
///////////////// google map start ///////////////////////////
////////////////////////////////////////////////////////	

jQuery(document).ready(function($) {			   
		
		$('#contactform').submit(function(){

			var action = $(this).attr('action');
	
			$("#message").slideUp(750,function() {
			$('#message').hide();
	
			$('button#submit').attr('disabled','disabled');
			$('img.loader').css('visibility', 'visible');
	
			$.post(action, {
				contact_name: $('#contact_name').val(),
				contact_email: $('#contact_email').val(),
				contact_phone: $('#contact_phone').val(),
				contact_message: $('#contact_message').val(),
				verify: $('#verify').val()
			},
				function(data){
					document.getElementById('message').innerHTML = data;
					$('#message').slideDown('slow');
					$('#contactform img.loader').css('visibility', 'hidden' );
					
					$('#submit').removeAttr('disabled');
					if(data.match('success') != null) $('#contactform').slideUp('slow');
	
				}
			);

		});

		return false;

	});
		
		
	});