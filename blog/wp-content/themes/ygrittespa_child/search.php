<?php global $wp_query;  
$options = _WSH()->option();

get_header(); 

$settings  = _WSH()->option(); 

$layout = sh_set( $settings, 'search_page_layout', 'right' );
$sidebar = sh_set( $settings, 'search_page_sidebar', 'blog-sidebar' );
$view = sh_set( $settings, 'search_page_view', 'list' );
_WSH()->page_settings = array('layout'=>$layout, 'view'=> $view, 'sidebar'=>$sidebar);

$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12' : ' col-lg-8 col-md-8 col-sm-12 col-xs-12' ;
?>

<!-- Blog -->
<section id="blog" class="blog single section">    
	<div class="container">
        <div class="row">
        	<div class="post-item page-title">
        		<h2><?php wp_title("");?></h2>
            </div>
        	<!-- sidebar area -->
			<?php if( $layout == 'left' ): ?>

            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">        
            	<div class="sidebar">
                	<?php dynamic_sidebar( $sidebar ); ?>
                </div>
            </div>

		    <?php endif; ?>
		    <!-- sidebar area -->	
            <!-- blog post area -->
            <?php if (have_posts()) : ?>
            <div class="<?php echo esc_attr($classes);?> blog_post_area">
            	<?php while( have_posts() ): the_post();?>
                	<!-- blog post item -->
                    <div id="post-<?php the_ID(); ?>" <?php post_class();?>>
                	<?php get_template_part( 'blog' ); ?>
                	<!-- blog post item -->
                    </div>
                <?php endwhile;?> 
                <!-- pagination -->
                <div class="post-nav wow fadeInRight" data-animation="fadeInUp" data-animation-delay="300">
                  <?php _the_pagination(); ?>
                </div>
                <!-- pagination -->
            
			</div>
            <?php else : ?>
					<div class="<?php echo esc_attr($classes);?> blog_post_area">
						<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', SH_NAME ); ?></p>
						<aside>
						<?php get_search_form(); ?>
						</aside>
					</div>
				<?php endif; ?>
            <!-- blog post area -->
            
          <!-- sidebar area -->
			<?php if( $layout == 'right' ): ?>

            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">        
            	<div class="sidebar">
                	<?php dynamic_sidebar( $sidebar ); ?>
                </div>
            </div>

		    <?php endif; ?>
		    <!-- sidebar area -->
            
        </div>
    
    </div>
</section><!-- Our Blog Section Ends -->

<?php get_footer(); ?>