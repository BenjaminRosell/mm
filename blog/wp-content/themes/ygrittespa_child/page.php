<?php $options = _WSH()->option();
get_header();

$settings  = sh_set(sh_set(get_post_meta(get_the_ID(), 'sh_page_meta', true) , 'sh_page_options') , 0);

$meta = _WSH()->get_meta('_sh_layout_settings');
$meta1 = _WSH()->get_meta('_sh_header_settings');

$layout = sh_set( $meta, 'layout', 'full' );
$sidebar = sh_set( $meta, 'sidebar', 'blog-sidebar' );

$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12' : ' col-lg-8 col-md-8 col-sm-12 col-xs-12';
?>

<!-- Blog -->
<section id="blog" class="blog single section">    
	<div class="container">
        <div class="row">
			<div class="post-item page-title">
        		<h2><?php wp_title("");?></h2>
            </div>
        	<!-- sidebar area -->
			<?php if( $layout == 'left' ): ?>

            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">        
            	<div class="sidebar">
                	<?php dynamic_sidebar( $sidebar ); ?>
                </div>
            </div>

		    <?php endif; ?>
		    <!-- sidebar area -->	
            <!-- blog post area -->
            <div class="<?php echo esc_attr($classes);?> blog_post_area">
            	<?php while( have_posts() ): the_post();?>
                	<div class="post-content wow fadeInUp">	
                    	<!-- blog post item -->
                		<?php the_content(); ?>
                		<!-- blog post item -->
                	</div>
				<?php endwhile;?> 
                <!-- pagination -->
                <div class="post-nav wow fadeInRight" data-animation="fadeInUp" data-animation-delay="300">
                  <?php _the_pagination(); ?>
                </div>
                <!-- pagination -->
            
			</div>
            <!-- blog post area -->
            
          <!-- sidebar area -->
			<?php if( $layout == 'right' ): ?>

            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">        
            	<div class="sidebar">
                	<?php dynamic_sidebar( $sidebar ); ?>
                </div>
            </div>

		    <?php endif; ?>
		    <!-- sidebar area -->
            
        </div>
    
    </div>
</section><!-- Our Blog Section Ends -->

<?php get_footer(); ?>