<?php
add_action('after_setup_theme', 'sh_theme_setup');
function sh_theme_setup()
{
	global $wp_version;
	if(!defined('SH_VERSION')) define('SH_VERSION', '1.0');
	if( !defined( 'SH_NAME' ) ) define( 'SH_NAME', 'wp_ygrittespa' );
	if( !defined( 'SH_ROOT' ) ) define('SH_ROOT', get_template_directory().'/');
	if( !defined( 'SH_URL' ) ) define('SH_URL', get_template_directory_uri().'/');	
	include_once( 'includes/loader.php' );
	
	load_theme_textdomain(SH_NAME, get_template_directory() . '/languages');
	add_editor_style();
	//ADD THUMBNAIL SUPPORT
	add_theme_support('post-thumbnails');
	add_theme_support('menus'); //Add menu support
	add_theme_support('automatic-feed-links'); //Enables post and comment RSS feed links to head.
	add_theme_support('widgets'); //Add widgets and sidebar support
	add_theme_support( 'woocommerce' );
	add_theme_support( "title-tag" );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	/** Register wp_nav_menus */
	if(function_exists('register_nav_menu'))
	{
		register_nav_menus(
			array(
				/** Register Main Menu location header */
				'main_menu' => __('Main Menu', SH_NAME),
				//'footer_menu' => __('Footer Menu', SH_NAME),
			)
		);
	}
	if ( ! isset( $content_width ) ) $content_width = 960;
	add_image_size( '201x201', 201, 201, true );
	add_image_size( '300x420', 300, 420, true );
	add_image_size( '340x250', 340, 250, true );
	add_image_size( '850x350', 850, 350, true );
	add_image_size( '82x84', 82, 84, true );
	
	
	
}
function sh_widget_init()
{
	global $wp_registered_sidebars;
	$theme_options = _WSH()->option();
	if( class_exists( 'SH_Recent_Post_With_Image' ) )register_widget( 'SH_Recent_Post_With_Image' );
	
	
	register_sidebar(array(
	  'name' => __( 'Default Sidebar', SH_NAME ),
	  'id' => 'default-sidebar',
	  'description' => __( 'Widgets in this area will be shown on the right-hand side.', SH_NAME ),
	  'class'=>'',
	  'before_widget'=>'<div id="%1$s" class="widget sidebar_widget wow fadeInUp %2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<h2 class="widget_title">',
	  'after_title' => '</h2>'
	));
	register_sidebar(array(
	  'name' => __( 'Footer Sidebar', SH_NAME ),
	  'id' => 'footer-sidebar',
	  'description' => __( 'Widgets in this area will be shown in Footer Area.', SH_NAME ),
	  'class'=>'',
	  'before_widget'=>'<article id="%1$s"  class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bottom_area_colum_1 wow fadeInUp %2$s">',
	  'after_widget'=>'</article>',
	  'before_title' => '<h2 class="footer_title">',
	  'after_title' => '</h2>'
	));
	
	register_sidebar(array(
	  'name' => __( 'Blog Listing', SH_NAME ),
	  'id' => 'blog-sidebar',
	  'description' => __( 'Widgets in this area will be shown on the right-hand side.', SH_NAME ),
	  'class'=>'',
	  'before_widget'=>'<div id="%1$s" class="widget sidebar_widget wow fadeInUp %2$s">',
	  'after_widget'=>'</div>',
	  'before_title' => '<h2 class="widget_title">',
	  'after_title' => '</h2>'
	));
	if( !is_object( _WSH() )  )  return;
	$sidebars = sh_set(sh_set( $theme_options, 'dynamic_sidebar' ) , 'dynamic_sidebar' ); 
	foreach( array_filter((array)$sidebars) as $sidebar)
	{
		if(sh_set($sidebar , 'topcopy')) continue ;
		
		$name = sh_set( $sidebar, 'sidebar_name' );
		
		if( ! $name ) continue;
		$slug = sh_slug( $name ) ;
		
		register_sidebar( array(
			'name' => $name,
			'id' =>  $slug ,
			'before_widget' => '<div id="%1$s" class="widget sidebar_widget wow fadeInUp %2$s">',
			'after_widget' => "</div>",
			'before_title' => '<h2 class="widget_title">',
			'after_title' => '</h2>',
		) );		
	}
	
	update_option('wp_registered_sidebars' , $wp_registered_sidebars) ;
}
add_action( 'widgets_init', 'sh_widget_init' );
// Update items in cart via AJAX
add_filter('add_to_cart_fragments', 'sh_woo_add_to_cart_ajax');
function sh_woo_add_to_cart_ajax( $fragments ) {
    
	global $woocommerce;
    
	ob_start();
	
	include('includes/modules/wc_cart.php' );
	
	$fragments['li.wc-header-cart'] = ob_get_clean();
	
    return $fragments;
}
add_filter( 'woocommerce_enqueue_styles', '__return_false' );
function _sh_animate_it( $atts, $contents = null )
{
	return include( 'includes/modules/shortcodes/animate_it.php' );
}
function load_head_scripts() {
    if ( !is_admin() ) {
    wp_register_script( 'map_api', 'http://maps.googleapis.com/maps/api/js?libraries=places&amp;sensor=false', '', '', false);
    wp_register_script( 'jquery-appear', get_template_directory_uri() . '/assets/plugins/animateNumber/jquery.appear.js', '', '', false);
    wp_register_script( 'jquery-animateNumber', get_template_directory_uri() . '/assets/plugins/animateNumber/jquery.animateNumber.min.js', '', '', false);
     wp_enqueue_script( 'map_api' );
     wp_enqueue_script( 'jquery-appear' );
     wp_enqueue_script( 'jquery-animateNumber' );
    }
    }
    add_action( 'wp_enqueue_scripts', 'load_head_scripts' );