<?php
class SH_Enqueue
{
	
	function __construct()
	{
		add_action( 'wp_enqueue_scripts', array( $this, 'sh_enqueue_scripts' ) );
		add_action( 'wp_head', array( $this, 'wp_head' ) );
		add_action( 'wp_footer', array( $this, 'wp_footer' ) );
		
		// Apply filter
		add_filter('body_class', array( $this, 'custom_body_classes') );
		
		add_action( '_sh_body_id', array( $this, 'body_id' ) );
		
	}
	function sh_enqueue_scripts()
	{
		global $post, $wp_query;
		$options = _WSH()->option();
		$current_theme = wp_get_theme();
		$header_style = sh_set( $options, 'header_style' );
		
		$version = $current_theme->get( 'Version' );
		
		$dark_color = ( sh_set( $options, 'website_theme' ) == 'dark' ) ? true : false;
		
		$dark_color = ( sh_set( $_GET, 'color_style' ) == 'dark' ) ? true : $dark_color;
		
		$protocol = is_ssl() ? 'https' : 'http';
		$styles = array( 'google_fonts' => $protocol.'://fonts.googleapis.com/css?family=Satisfy',
						 'google_fonts2' => $protocol.'://fonts.googleapis.com/css?family=Open+Sans:400italic,800italic,400,300,600,700,800', 	
						 'bootstrap' => 'assets/plugins/bootstrap-3.3.1/css/bootstrap.min.css',
						 'font-awesome' => 'assets/plugins/font-awesome/css/font-awesome.min.css',
						 'animate' => 'assets/plugins/animate/animate.css',
						 'prettyPhoto' => 'assets/plugins/prettyPhoto/prettyPhoto.css',
						 'owl-carousel' => 'assets/plugins/owl-carousel/owl.carousel.css',
						 'owl-theme' => 'assets/plugins/owl-carousel/owl.theme.css',
						 'hover-min' => 'assets/plugins/hover-css/hover-min.css',
						 
						 'main-style'	=> 'style.css',
						 'custom-style'	=> 'assets/css/custom.css',
						 'color_scheme' => (sh_set($options, 'implement_color_scheme')) ? 'assets/css/colors.css' : '' ,
						 
						 );
		
		$styles = $this->custom_fonts($styles); //Load google fonts that user choose from theme options
		
							
		foreach( $styles as $name => $style )
		{
			if( !$style ) continue;
			if(strstr($style, 'http') || strstr($style, 'https') ) wp_enqueue_style( $name, $style);
			else wp_enqueue_style( $name, _WSH()->includes( $style, true ), '', $version );
		}
		
		$scripts = array( 
						  'bootstrap' => 'assets/plugins/bootstrap-3.3.1/js/bootstrap.min.js',
						  
						  
						  'smooth-scroll' => 'assets/plugins/smooth-scroll/smooth-scroll.js',
						  'jquery-isotope' => 'assets/plugins/isotope/jquery.isotope.min.js',
						  'jquery-wow'	=>	'assets/plugins/wow/wow.min.js',
						  'jquery-hcaptions'	=> 'assets/plugins/hcaptions/jquery.hcaptions.js',
						  'prettyPhoto'	=> 'assets/plugins/prettyPhoto/jquery.prettyPhoto.js',
						  
						  'owl-carousel'	=> 'assets/plugins/owl-carousel/owl.carousel.js',
						  'jquery-validate'		=> 'assets/plugins/jquery-validate/jquery.validate.min.js',
						  
						  'main_script' => 'assets/js/app.js',
						  'jquery-cookie' => 'js/jquery.cookie.js',
						  'theme-panel' => 'js/themepanel.js',
						 );
		foreach( $scripts as $name => $js )
		{
			wp_register_script( $name, _WSH()->includes( $js, true ), '', $version, true);
		}
		if( is_home() || is_front_page() ) wp_enqueue_script( array('jquery-nav','jquery-scroll'));
		if(sh_set($options, 'preloader'))
		wp_enqueue_script( array('jquery', 'bootstrap', 'smooth-scroll', 'jquery-isotope', 'jquery-wow', 'jquery-hcaptions', 'prettyPhoto', 'owl-carousel', 'jquery-appear', 'animateNumber', 'main_script'));
		else
		wp_enqueue_script( array('jquery', 'bootstrap', 'smooth-scroll', 'jquery-isotope', 'jquery-wow', 'jquery-hcaptions', 'prettyPhoto', 'owl-carousel', 'jquery-appear', 'animateNumber', 'main_script'));
		if( is_singular() ) wp_enqueue_script('comment-reply');
		if(sh_set($options, 'switcher')) wp_enqueue_script(array('jquery-cookie', 'theme-panel'));
		
		if( is_single() ) {
			$format = get_post_format();
			if( $format == 'gallery' ) wp_enqueue_script( array( 'jquery-flexslider' ) );
			if( $format == 'video' || $format == 'audio' ) wp_enqueue_script( array( 'jquery-fitvids' ) );
		}
		if( is_singular( 'sh_projects' ) || $wp_query->is_posts_page || is_search() || is_tag() || is_category() || is_author() || is_archive() ) 
  		wp_enqueue_script( array('jquery-flexslider', 'owl.carousel', 'jquery-fitvids') );
		wp_enqueue_script( array('custom_script') );
		
		
	}
	
	function wp_head()
	{
		$opt = _WSH()->option();
		$boxed = sh_set( $opt, 'boxed_layout' );
		$boxed_style = ( $boxed && sh_set( $opt, 'bg_boxed' ) ) ? ' body { background: url('.sh_set( $opt, 'bg_boxed').') repeat center center; }' : '';
		
		if( is_page() ) {
			$meta = _WSH()->get_meta();//printr($meta);
			$boxed = (sh_set( $meta, 'boxed' )) ? true : $boxed;
			$boxed_style = ( $boxed && sh_set( $meta, 'bg_boxed' ) ) ? ' body { background: url('.sh_set( $meta, 'bg_boxed').') repeat center center; }' : '';
		}
		
		echo '<script type="text/javascript"> if( ajaxurl === undefined ) var ajaxurl = "'.admin_url('admin-ajax.php').'";</script>';?>
		<style type="text/css">
			<?php
			if( sh_set( $opt, 'body_custom_font') )
			echo sh_get_font_settings( array( 'body_font_size' => 'font-size', 'body_font_family' => 'font-family', 'body_font_style' => 'font-style', 'body_font_color' => 'color', 'body_line_height' => 'line-height' ), 'body, p {', '}' );
			
			if( sh_set( $opt, 'use_custom_font' ) ){
				echo sh_get_font_settings( array( 'h1_font_size' => 'font-size', 'h1_font_family' => 'font-family', 'h1_font_style' => 'font-style', 'h1_font_color' => 'color', 'h1_line_height' => 'line-height' ), 'h1 {', '}' );
				echo sh_get_font_settings( array( 'h2_font_size' => 'font-size', 'h2_font_family' => 'font-family', 'h2_font_style' => 'font-style', 'h2_font_color' => 'color', 'h2_line_height' => 'line-height' ), 'h2 {', '}' );
				echo sh_get_font_settings( array( 'h3_font_size' => 'font-size', 'h3_font_family' => 'font-family', 'h3_font_style' => 'font-style', 'h3_font_color' => 'color', 'h3_line_height' => 'line-height' ), 'h3 {', '}' );
				echo sh_get_font_settings( array( 'h4_font_size' => 'font-size', 'h4_font_family' => 'font-family', 'h4_font_style' => 'font-style', 'h4_font_color' => 'color', 'h4_line_height' => 'line-height' ), 'h4 {', '}' );
				echo sh_get_font_settings( array( 'h5_font_size' => 'font-size', 'h5_font_family' => 'font-family', 'h5_font_style' => 'font-style', 'h5_font_color' => 'color', 'h5_line_height' => 'line-height' ), 'h5 {', '}' );
				echo sh_get_font_settings( array( 'h6_font_size' => 'font-size', 'h6_font_family' => 'font-family', 'h6_font_style' => 'font-style', 'h6_font_color' => 'color', 'h6_line_height' => 'line-height' ), 'h6 {', '}' );
			}
			echo balanceTags($boxed_style);			
			echo sh_set( $opt, 'header_css' );
			?>
		</style>
        
        <?php if(function_exists('sh_theme_color_scheme')) sh_theme_color_scheme(); ?>
		
		<?php if( sh_set( $opt, 'header_js' ) ): ?>
			<script type="text/javascript">
                <?php echo sh_set( $opt, 'header_js' );?>
            </script>
        <?php endif;?>
        <?php
	}
	
	function wp_footer()
	{
		$analytics = sh_set( _WSH()->option(), 'footer_analytics');
		
		echo balanceTags($analytics);
		
		$theme_options = _WSH()->option();
		
		if( sh_set( $theme_options, 'footer_js' ) ): ?>
			<script type="text/javascript">
                <?php echo sh_set( $theme_options, 'footer_js' );?>
            </script>
        <?php endif;
	}
	
	function custom_fonts( $styles )
	{
		$opt = _WSH()->option();
		$protocol = ( is_ssl() ) ? 'https' : 'http';
		$font = array();
		//$font_options = array('h1_font_family', 'h2_font_family', 'h3_font_family');
		
		if( sh_set( $opt, 'use_custom_font' ) ){
			
			if( $h1 = sh_set( $opt, 'h1_font_family' ) ) $font[$h1] = urlencode( $h1 ).':400,300,600,700,800';
			if( $h2 = sh_set( $opt, 'h2_font_family' ) ) $font[$h2] = urlencode( $h2 ).':400,300,600,700,800';
			if( $h3 = sh_set( $opt, 'h3_font_family' ) ) $font[$h3] = urlencode( $h3 ).':400,300,600,700,800';
		}
		
		if( sh_set( $opt, 'body_custom_font' ) ){
			if( $body = sh_set( $opt, 'body_font_family' ) ) $font[$body] = urlencode( $body ).':400,300,600,700,800';
		}
		
		if( $font ) $styles['sh_google_custom_font'] = $protocol.'://fonts.googleapis.com/css?family='.implode('|', $font);
		
		return $styles;
	}
	
	function custom_body_classes( $classes )
	{
		$options = _WSH()->option();
		
		$dark_color = ( sh_set( $options, 'website_theme' ) == 'dark' ) ? true : false;
		
		$dark_color = ( sh_set( $_GET, 'color_style' ) == 'dark' ) ? true : $dark_color;
		
		if( $dark_color ) $classes[] = 'dark-style';
	
		return $classes;
	}
	
	function body_id() 
	{
		$options = _WSH()->option();
		
		$boxed = sh_set( $options, 'boxed_layout' );
		
		$boxed_layout = ( $boxed && !$nobg ) ? ' id="boxed" ' : ''; 
		
		echo balanceTags($boxed_layout);
	}
}