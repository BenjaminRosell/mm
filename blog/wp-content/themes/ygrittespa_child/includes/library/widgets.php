<?php
/// Recent Posts 
class SH_Recent_Post_With_Image extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Recent_Post_With_Image', /* Name */__('Spa Recent Posts with image',SH_NAME), array( 'description' => __('Show the recent posts with images', SH_NAME )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo balanceTags($before_widget); ?>
		
		<div class="blog/popular-post wow fadeInUp">
        
		<?php echo balanceTags($before_title.$title.$after_title); ?>
		
		<?php $query_string = 'posts_per_page='.$instance['number'];
		if( $instance['cat'] ) $query_string .= '&cat='.$instance['cat'];
		query_posts( $query_string ); 
		
		$this->posts();
		wp_reset_query();
		?>
        
        </div> 
		
		<?php echo balanceTags($after_widget);
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : __('Recently Added', SH_NAME);
		$number = ( $instance ) ? esc_attr($instance['number']) : 5;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title: ', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('number')); ?>"><?php _e('No. of Posts:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('number')); ?>" name="<?php echo esc_attr($this->get_field_name('number')); ?>" type="text" value="<?php echo esc_attr( $number ); ?>" />
        </p>
       
    	<p>
            <label for="<?php echo esc_attr($this->get_field_id('cat')); ?>"><?php _e('Category', SH_NAME); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>__('All Categories', SH_NAME), 'selected'=>$cat, 'class'=>'widefat', 'name'=>$this->get_field_name('cat')) ); ?>
        </p>
            
		<?php 
	}
	
	function posts()
	{
		
		if( have_posts() ):?>
        
           	<!-- Title -->
				
                <ul class="popular-list list-unstyled">
				<?php while( have_posts() ): the_post(); ?>
                    
                    <!-- Item -->
                    <li>
                        <!-- Post Image -->
                        <a href="<?php the_permalink();?>"><?php the_post_thumbnail('82x84');?></a>
                        <!-- Details -->
                        <div class="content">
                            <h3><a href="<?php the_permalink();?>"><?php echo _sh_trim(get_the_title(), 4);?></a></h3>
                            <div class="posted-date"><?php echo get_the_date('F d, Y');?></div>
                        </div>
                    </li>
                <?php endwhile; ?>
                </ul>
            
        <?php endif;
    }
}