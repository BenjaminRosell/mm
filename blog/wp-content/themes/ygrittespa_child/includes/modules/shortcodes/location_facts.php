<?php ob_start(); ?>

<div class="locations">
	<div class="container">
    	<div class="row">
        	<?php echo do_shortcode( $contents );?>
        </div>
    </div>
</div> 

<?php return ob_get_clean(); ?>