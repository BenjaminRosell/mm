<?php global $wp_query;
$paged = get_query_var('paged');
$args = array('post_type' => 'sh_projects', 'showposts'=>$num, 'orderby'=>$sort, 'order'=>$order, 'paged'=>$paged);
if( $cat ) $args['tax_query'] = array(array('taxonomy' => 'projects_category','field' => 'id','terms' => $cat));
query_posts($args);


$t = $GLOBALS['_sh_base'];
$paged = get_query_var('paged');

$data_filtration = '';
$data_posts = '';
?>


<?php if( have_posts() ):
	
ob_start();?>

	<?php $count = 0; 
	$fliteration = array();?>
	<?php while( have_posts() ): the_post();
		global  $post;
		$meta = get_post_meta( get_the_id(), '_sh_portfolio_meta', true );
		$post_terms = get_the_terms( get_the_id(), 'projects_category');
		foreach( (array)$post_terms as $pos_term ) $fliteration[$pos_term->term_id] = $pos_term;
		$temp_category = get_the_term_list(get_the_id(), 'projects_category', '', ', ');
	?>
		<?php $post_terms = wp_get_post_terms( get_the_id(), 'projects_category'); 
		$term_slug = '';
		if( $post_terms ) foreach( $post_terms as $p_term ) $term_slug .= $p_term->slug.' ';?>		
                
		   <li class="portfolio-item <?php echo esc_attr($term_slug); ?>">
                <div class="item-main">
                    <div class="portfolio-image">
                        <?php the_post_thumbnail( '340x250' );?>
                        <div class="overlay">
                            <?php 
                                $post_thumbnail_id = get_post_thumbnail_id($post->ID);
                                $post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
                            ?>
                            <a class="preview btn" title="Image Title Here" href="<?php echo esc_url($post_thumbnail_url);?>"><i class="fa fa-arrows-alt"></i></a>
                        </div>
                    </div>
                </div>
            </li>

<?php endwhile;?>
  
<?php wp_reset_query();
$data_posts = ob_get_contents();
ob_end_clean();

endif; 

ob_start();?>	 

<?php $terms = get_terms(array('projects_category')); ?>
<?php if( $terms ): ?>
<ul class="portfolio-filter wow bounceIn" data-wow-duration="0.8s" data-wow-delay="0.8s">
    <li><a class="btn active margin-5" href="#" data-filter="*"><?php _e('All', SH_NAME);?></a></li>
    <?php foreach( $fliteration as $t ): ?>
    	<li><a class="btn margin-5" href="#" data-filter=".<?php echo sh_set( $t, 'slug' ); ?>"><?php echo sh_set( $t, 'name'); ?></a></li>
    <?php endforeach;?>
    
</ul>
<?php endif;?>
<div class="portfolio-grid">
	<ul class="portfolio-items col-4">
    	<?php echo balanceTags($data_posts); ?>
    </ul>
</div>

<?php $output = ob_get_contents();
ob_end_clean(); 
return $output;?>