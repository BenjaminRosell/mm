<?php   
ob_start();
?>
<!-- Map Area -->
<div class="map">
 <div id="contact_map"></div>
</div>
<script type="text/javascript">
google.maps.event.addDomListener(window, 'load', init);

var map;

function init() {
    var mapOptions = {
        center: new google.maps.LatLng(<?php echo esc_js($lat);?>, <?php echo esc_js($long);?>),
        zoom: 15,
        zoomControl: false,
		disableDefaultUI: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.DEFAULT,
        },
        disableDoubleClickZoom: false,
        mapTypeControl: false,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        },
        scaleControl: true,
        scrollwheel: false,
        streetViewControl: false,
        draggable : true,
        overviewMapControl: false,
    }

    var mapElement = document.getElementById('contact_map');
    var map = new google.maps.Map(mapElement, mapOptions);
    var locations = [
        ['', <?php echo esc_js($mark_lat);?>, <?php echo esc_js($mark_long);?>]
    ];
	var i= 0;
	var marker;
    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            icon: '<?php echo get_template_directory_uri();?>/assets/images/map-pin.png',
//            animation: google.maps.Animation.BOUNCE,
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
        });
    }
    
	// Create an InfoWindow for the marker
		var contentString = "<p class='map-info'><strong><?php echo esc_js($mark_title);?></strong> <br/><?php echo esc_js($mark_address);?><br/> <?php echo esc_js($mark_phone);?></p>";	// HTML text to display in the InfoWindow
		var infowindow = new google.maps.InfoWindow( { content: contentString } );
		
		// Set event to display the InfoWindow anchored to the marker when the marker is clicked.
		google.maps.event.addListener( marker, 'click', function() { infowindow.open( map, marker ); });
	
}
</script>
<?php return ob_get_clean();?>		