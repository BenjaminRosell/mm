<?php  
   $count = 1;
   $query_args = array('post_type' => 'sh_testimonials' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   
   if( $cat ) $query_args['testimonials_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   
   ob_start() ;?>
   
<?php if($query->have_posts()):  ?>   

<div class="clients-grid">
	
	<?php while($query->have_posts()): $query->the_post();
		global $post ; 
		$clients_meta = _WSH()->get_meta();
	?>
    
    <div class="media wow <?php if(($count%2) == 0 && $count != 1) echo "fadeInRight"; else echo "fadeInLeft";?>" data-wow-duration="0.9s" data-wow-delay="0.9s">
      <div class="<?php if(($count%2) == 0 && $count != 1) echo "media-right"; else echo "media-left";?>">
        <a href="<?php echo sh_set($clients_meta, 'ext_url');?>" class="hvr-overline-from-center">
          <?php the_post_thumbnail('201x201');?>
        </a>
      </div>
      <div class="media-body">
        <p><?php echo _sh_trim(get_the_content(), $text_limit);?></p>
      </div>
      <div class="row">
        <div class="col-md-12">
            <div class="<?php if(($count%2) == 0 && $count != 1) echo "media-label right"; else echo "media-label";?>">
                <p><?php the_title();?></p>
                <p><strong><?php echo sh_set($clients_meta, 'designation');?></strong></p>
            </div>
        </div>
      </div>
    </div>
    
    <?php $count++; endwhile; ?>
    
</div>

<?php endif; ?>

<?php 
	wp_reset_query();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>