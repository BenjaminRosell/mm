<?php ob_start(); ?>

<li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s">
    <span id="number1">0</span>
    <h4><?php echo balanceTags($bigword);?> </h4>
    <h5><?php echo balanceTags($smallword);?></h5>
</li>

<?php return ob_get_clean(); 

