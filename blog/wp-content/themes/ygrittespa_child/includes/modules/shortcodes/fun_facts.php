<?php ob_start(); ?>

<div class="stats">
    <div class="container">
        <ul class="list-inline" id="numbers">
        	<?php echo do_shortcode( $contents );?>	
        </ul>
    </div>
</div>

<?php return ob_get_clean(); ?>