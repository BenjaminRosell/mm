<?php ob_start(); ?>

<div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
    <h4><?php echo balanceTags($title);?></h4>
    <ul class="list-unstyled">
      <?php if($address):?><li><?php echo balanceTags($address);?></li><?php endif;?>
      <?php if($phone):?><li><?php echo balanceTags($phone);?></li><?php endif;?>
      <?php if($email):?><li><a href="mailto:<?php echo esc_url($email);?>"><?php echo balanceTags($email);?></a></li><?php endif;?>
    </ul>
</div>

<?php return ob_get_clean(); 

