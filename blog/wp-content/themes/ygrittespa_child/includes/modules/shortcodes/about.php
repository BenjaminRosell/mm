<?php  
ob_start() ;?>

<div class="both">
<div class="col-md-6 col-sm-12 text-left wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.8s">
    <h3><?php echo balanceTags($title);?></h3>
    <p><?php echo balanceTags($text);?></p>
    <ul class="list-unstyled arrow-list">
      <?php $fearures = explode("\n",$feature_str);?>
      
	  <?php foreach($fearures as $feature):?>
      	  
          <li><i class="fa fa-chevron-circle-right"></i> <?php echo balanceTags($feature);?></li>
      
	  <?php endforeach;?>
   
    </ul>
    
</div>
<div class="col-md-6 col-sm-12 wow bounceIn about-img" data-wow-duration="0.9s" data-wow-delay="0.9s">
                
    <div class="circle-img green">
        <img src="<?php echo wp_get_attachment_url( $img1, 'full' ); ?>" class="hvr-rotate" alt=" green ">
    </div>
    <div class="circle-img blue">
        <img src="<?php echo wp_get_attachment_url( $img2, 'full' ); ?>" class="hvr-rotate" alt=" blue ">
    </div>
</div>
</div>                

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   