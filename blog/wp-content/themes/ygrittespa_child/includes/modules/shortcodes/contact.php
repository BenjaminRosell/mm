<?php
   ob_start();
   ?>
<div class="container">
	<div id="message"></div>
    <form class="form-horizontal contact-form" role="form" name="contactform" method="post" id="contactform" action="<?php echo admin_url( 'admin-ajax.php?action=_sh_ajax_callback&amp;subaction=contact_form_submit'); ?>">
    <div class="row">
        <div class="col-md-6 wow fadeInLeft"  data-wow-duration="0.9s" data-wow-delay="0.9s">
            <div class="form-group">
                <div class="col-sm-12">
                  <input type="text" class="form-control" name="contact_name" id="contact_name" placeholder="<?php _e('Your name', SH_NAME);?>">
                </div>
              </div>
            <div class="form-group">
                <div class="col-sm-12">
                  <input type="email" class="form-control" name="contact_email" id="contact_email" placeholder="<?php _e('Your email address', SH_NAME);?>">
                </div>
              </div>
            <div class="form-group">
                <div class="col-sm-12">
                  <input type="text" class="form-control" name="contact_phone" id="contact_phone" placeholder="<?php _e('Your phone number', SH_NAME);?>">
                </div>
              </div>
        </div>
        <div class="col-md-6 wow fadeInRight" data-wow-duration="0.9s" data-wow-delay="0.9s">
            <div class="form-group">
            <div class="col-sm-12">
              <textarea class="form-control" placeholder="Your message" name="contact_message" id="contact_message" rows="3"></textarea>
            </div>
          </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12 text-center">
          <button type="submit" class="btn main-btn hvr-sweep-to-right"><i class="fa fa-angle-right"></i><?php _e(' Send Message', SH_NAME);?></button>
        </div>
      </div>
    </form>
</div>   
   
<?php return ob_get_clean();?>		