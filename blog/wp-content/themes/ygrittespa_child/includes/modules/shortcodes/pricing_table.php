<?php ob_start(); ?>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="plan wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.8s">
        <div class="flower"></div>
        <h4 class="plan-name"><?php echo balanceTags($title);?></h4>
        <ul class="list-unstyled">
        <?php $fearures = explode("\n",$feature_str);?>
        <?php foreach($fearures as $feature):?>
        <?php $fearures_inner = explode("||",$feature);?>
        	    <li>
                    <p><?php echo balanceTags($fearures_inner[0]);?></p>
                    <strong><?php echo balanceTags($fearures_inner[1]);?></strong>
                </li>
        <?php endforeach;?>    
    	</ul>
        <div class="price-wrap">
            <div class="price-box">
                <strong><?php echo balanceTags($price);?></strong>
                <p><?php echo balanceTags($package_duration);?></p>
            </div>
        </div>
        <a href="<?php echo esc_url($btn_link);?>" class="btn price-btn btn-lg"><i class="fa fa-angle-right"></i><?php echo balanceTags($btn_text);?></a>
</div>
</div>

<?php return ob_get_clean(); 