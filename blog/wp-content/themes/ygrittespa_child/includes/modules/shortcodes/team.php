<?php  
   $count = 1;
   $query_args = array('post_type' => 'sh_team' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['team_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   
   ob_start() ;?>
   
<?php if($query->have_posts()):  ?>
<div class="members-grid">
    <?php while($query->have_posts()): $query->the_post();
            global $post ; 
            $teams_meta = _WSH()->get_meta();
	?>
    <div class="col-md-3 col-sm-6 ">
        <div class="thumbnail wow fadeInLeft" data-wow-duration="0.9s" data-wow-delay="0.9s">
            <div class="img-wrap hvr-rotate">
                <a href="<?php echo sh_set($teams_meta, 'team_link');?>" class="hvr-reveal"><?php the_post_thumbnail('201x201');?></a>
            </div>
      <div class="caption">
        <h3><?php the_title();?></h3>
        <h4><?php echo sh_set($teams_meta, 'designation');?></h4>
        <p><?php echo _sh_trim(get_the_excerpt(),$text_limit);?></p>
        <?php if($social = sh_set($teams_meta, 'sh_team_social')): ?>
        <ul class="list-inline social">
          
		  <?php foreach($social as $key => $val):?>	
          	<li><a href="<?php echo sh_set($val, 'social_link');?>"><i class="<?php echo sh_set($val, 'social_icon');?>"></i></a></li>
          <?php endforeach;?>
          
        </ul>
        <?php endif;?>
      </div>
    </div>
    </div>
    
    <?php endwhile;?>
    
</div>

<?php endif; ?>

<?php 
	wp_reset_query();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>