<?php  
ob_start() ;?>

<article <?php if($id):?>id="<?php echo balanceTags($id);?>"<?php endif;?>>
	<div class="container">
        <div class="row">
            	<h2 class="heading wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s"><?php echo balanceTags($title);?></h2>
                <div class="separator_wrapper wow bounceIn" data-wow-duration="0.7s" data-wow-delay="0.7s">
                        <div class="separator_first_circle">
                        <img src="<?php echo get_template_directory_uri();?>/assets/images/green-flower.png" alt="green flower">
                        </div>
                    </div>
                <p class="helping-text wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0.8s"><?php echo balanceTags($text);?></p>
        </div>
    </div>
</article>

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   