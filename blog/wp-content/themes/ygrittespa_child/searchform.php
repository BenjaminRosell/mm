<div class="search wow fadeInUp">
<form action="<?php echo home_url(); ?>" method="get" class="search_form">
	<input type="search" name="s" placeholder="<?php _e('SEARCH..',  SH_NAME); ?>" value="<?php echo get_search_query(); ?>">
    <input type="submit" value="<?php _e('submit', SH_NAME);?>">
</form>
</div>