<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains comments and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() )
	return;
?>
<div itemscope itemtype="http://schema.org/Comment" id="comments" class="post-comments comment-area clearfix">
	<?php if ( have_comments() ) : ?>
		<div class="title-head wow fadeInUp"><?php comments_number();?></div>
        <p><?php _e('You are not signed in. ', SH_NAME);?><a href="<?php echo home_url(); ?>/wp-login.php" title="sign in"><?php _e('Sign in', SH_NAME);?></a><?php _e(' to post comments.', SH_NAME);?></p>
		
        <!-- comments -->
		<div class="comment-list">
				<?php
					wp_list_comments( array(
						'style'       => 'ul',
						'short_ping'  => true,
						'avatar_size' => 74,
						'callback'=>'sh_list_comments'
					) );
				?>
		</div>
		
		<?php
			// Are there comments to navigate through?
			if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
		?>
		<nav class="navigation comment-navigation" role="navigation">
			<h1 class="screen-reader-text section-heading"><?php _e( 'Comment navigation', SH_NAME ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', SH_NAME ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', SH_NAME ) ); ?></div>
		</nav><!-- .comment-navigation -->
		<?php endif; // Check for comment navigation ?>
		<?php if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="no-comments"><?php _e( 'Comments are closed.' , SH_NAME ); ?></p>
		<?php endif; ?>
	<?php endif; // have_comments() ?>
		<!-- Add Your Comments -->
        <div class="comments-form wow fadeInUp">
            <!-- Heading -->
            <div class="title-head"><?php _e('add your comment', SH_NAME);?></div>
			<?php sh_comment_form(); ?>
        </div>    
</div><!-- #comments -->
