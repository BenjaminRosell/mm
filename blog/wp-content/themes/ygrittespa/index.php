<?php global $wp_query; 
$options = _WSH()->option();

get_header(); 
if( $wp_query->is_posts_page ) {
	$meta = _WSH()->get_meta('_sh_layout_settings', get_queried_object()->ID);
	$layout = sh_set( $meta, 'layout', 'right' );
	$sidebar = sh_set( $meta, 'sidebar', 'default-sidebar' );
	$view = sh_set( $meta, 'view', 'grid' );
} else {

	$settings  = _WSH()->option(); 
	$layout = sh_set( $settings, 'archive_page_layout', 'right' );
	$sidebar = sh_set( $settings, 'archive_page_sidebar', 'default-sidebar' );
	$view = sh_set( $settings, 'archive_page_view', 'list' );
}

$layout = sh_set( $_GET, 'layout' ) ? sh_set( $_GET, 'layout' ) : $layout;
$view = sh_set( $_GET, 'view' ) ? sh_set( $_GET, 'view' ) : $view;
$sidebar = ( $sidebar ) ? $sidebar : 'default-sidebar';
_WSH()->page_settings = array('layout'=>'right', 'view'=> $view, 'sidebar'=>$sidebar);
$classes = ( !$layout || $layout == 'full' ) ? ' col-lg-12 col-md-12 col-sm-12 col-xs-12' : ' col-lg-8 col-md-8 col-sm-12 col-xs-12';

?>
<!-- Blog -->
<section id="blog" class="blogm single section">    
	<div class="container">
        <div class="row">
        	<div class="post-item page-title">
        		<h2><?php wp_title("");?></h2>
            </div>
        	<!-- sidebar area -->
			<?php if( $layout == 'left' ): ?>

            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">        
            	<div class="sidebar">
                	<?php dynamic_sidebar( $sidebar ); ?>
                </div>
            </div>

		    <?php endif; ?>
		    <!-- sidebar area -->	
            <!-- blog post area -->
            <div class="<?php echo esc_attr($classes);?> blog_post_area">
            	<?php while( have_posts() ): the_post();?>
                	<!-- blog post item -->
                    <div id="post-<?php the_ID(); ?>" <?php post_class();?>>
                	<?php get_template_part( 'blog' ); ?>
                	<!-- blog post item -->
                    </div>
                <?php endwhile;?> 
                <!-- pagination -->
                <div class="post-nav wow fadeInRight" data-animation="fadeInUp" data-animation-delay="300">
                  <?php _the_pagination(); ?>
                </div>
                <!-- pagination -->
            
			</div>
            <!-- blog post area -->
            
          <!-- sidebar area -->
			<?php if( $layout == 'right' ): ?>

            <div id="sidebar" class="col-lg-4 col-md-4 col-sm-12 col-xs-12">        
            	<div class="sidebar">
                	<?php dynamic_sidebar( $sidebar ); ?>
                </div>
            </div>

		    <?php endif; ?>
		    <!-- sidebar area -->
            
        </div>
    
    </div>
</section><!-- Our Blog Section Ends -->


<?php get_footer(); ?>