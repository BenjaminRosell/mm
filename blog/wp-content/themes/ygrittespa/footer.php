<?php $options = get_option(SH_NAME.'_theme_options');  ?>
<footer>
	<div class="container">
    	<ul class="list-inline social">
          <?php if(sh_set($options, 'footer_facebook')):?><li class="wow bounceIn" data-wow-duration="0.5s" data-wow-delay="0.5s"><a href="<?php echo esc_url(sh_set($options, 'footer_facebook'));?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/social-icons/facebook.png" alt="green flower"></a></li><?php endif;?>
          <?php if(sh_set($options, 'footer_twitter')):?><li class="wow bounceIn" data-wow-duration="0.6s" data-wow-delay="0.6s"><a href="<?php echo esc_url(sh_set($options, 'footer_twitter'));?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/social-icons/twitter.png" alt="green flower"></a></li><?php endif;?>
          <?php if(sh_set($options, 'footer_google_plus')):?><li class="wow bounceIn" data-wow-duration="0.7s" data-wow-delay="0.7s"><a href="<?php echo esc_url(sh_set($options, 'footer_google_plus'));?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/social-icons/googleplus.png" alt="green flower"></a></li><?php endif;?>
          <?php if(sh_set($options, 'footer_flickr')):?><li class="wow bounceIn" data-wow-duration="0.8s" data-wow-delay="0.8s"><a href="<?php echo esc_url(sh_set($options, 'footer_flickr'));?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/social-icons/flickr.png" alt="green flower"></a></li><?php endif;?>
          <?php if(sh_set($options, 'footer_linkedin')):?><li class="wow bounceIn" data-wow-duration="0.9s" data-wow-delay="0.9s"><a href="<?php echo esc_url(sh_set($options, 'footer_linkedin'));?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/social-icons/linkedin.png" alt="green flower"></a></li><?php endif;?>
          <?php if(sh_set($options, 'footer_vimeo')):?><li class="wow bounceIn" data-wow-duration="1s" data-wow-delay="1s"><a href="<?php echo esc_url(sh_set($options, 'footer_vimeo'));?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/social-icons/vimeo.png" alt="green flower"></a></li><?php endif;?>
          <?php if(sh_set($options, 'footer_youtube')):?><li class="wow bounceIn" data-wow-duration="1.1s" data-wow-delay="1.1s"><a href="<?php echo esc_url(sh_set($options, 'footer_youtube'));?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/social-icons/youtube.png" alt="green flower"></a></li><?php endif;?>
          <?php if(sh_set($options, 'footer_sharethis')):?><li class="wow bounceIn" data-wow-duration="1.2s" data-wow-delay="1.2s"><a href="<?php echo esc_url(sh_set($options, 'footer_sharethis'));?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/social-icons/sharethis.png" alt="green flower"></a></li><?php endif;?>
        </ul>
    </div>
    <div class="foot-line">
    	<p><?php echo sh_set($options, 'copy_right');?></p>
    </div>
</footer>
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
<?php if(sh_set($options, 'switcher')):?>
<div class="switcher">
        <i class="fa fa-cog"></i>
        <strong>Layout</strong>
        <div class=""><a href="#" id="full">Full</a></div>
        <div class=""><a href="#" id="boxed">Box</a></div>
        <strong>Colors</strong>
        <div class="box" title="default" id="default">default</div>
        <div class="box" title="Asparagus" id="asparagus">Aspara</div>
        <div class="box" title="Green" id="green">green</div>
        <div class="box" title="Orange" id="orange">orange</div>
        <div class="box" title="Purple" id="purple">purple</div>
        <div class="box" title="Yellow" id="yellow">yellow</div>
        <div class="box" title="Tomato" id="tomato">Tomato</div>
        <div class="box" title="Teal" id="teal">Teal</div>
        <div class="box" title="Pink" id="pink">Pink</div>
        <div class="box" title="lima" id="lima">Lima</div>
        <div class="box" title="Blue" id="blue">blue</div>
        <div class="box" title="turquoise" id="turquoise">Turquoise</div>
    </div>
<?php endif;?>    
<?php wp_footer(); ?>

</body>

</html>