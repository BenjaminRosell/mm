<?php $options = _WSH()->option();
Global $wp_query;
 ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <!--<![endif]-->
<head>
	 <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(''); ?></title>
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Favcon -->
	<?php if( sh_set( $options, 'site_favicon' ) ):?>
    	<link rel="shortcut icon" type="image/png" href="<?php echo sh_set( $options, 'site_favicon' );?>">
    <?php else:?> 
    	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png" type="image/x-icon"/>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png" type="image/x-icon"/>
	<?php endif;?>
	<script src="<?php echo get_template_directory_uri();?>/assets/js/modernizr.js"></script>
    <!--[if lt IE 9]>
          <script src="<?php echo get_template_directory_uri();?>/assets/js/html5shiv.min.js"></script>
          <script src="<?php echo get_template_directory_uri();?>/assets/js/respond.min.js"></script>
        <![endif]-->
	
	<?php wp_head(); ?>
</head>
<body <?php if( sh_set($options, 'boxed') ) body_class('boxed'); else body_class(); ?>>
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- Fixed navbar -->
<div class="navbar main-menu navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <?php if(sh_set($options, 'logo_image')):?>
        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="logo"><img src="<?php echo sh_set($options, 'logo_image');?>" alt="logo" /></a>
      <?php else:?>
        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="logo"><img src="<?php echo get_template_directory_uri();?>/assets/images/logo.png" alt="logo" /></a>
      <?php endif;?>
	</div>
    <div class="navbar-collapse collapse">
      <?php do_action( 'sh_header_menus' ); ?>	
    </div>
    <!--/.nav-collapse --> 
  </div>
</div>