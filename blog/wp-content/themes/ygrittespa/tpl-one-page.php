<?php /* Template Name: OnePage */
get_header(); 

$theme_options = _WSH()->option(); 
$single_pages = sh_set( $theme_options, 'single_pages' );
if( $single_pages ):?>

<?php $count = 1;
foreach((array) $single_pages as $page_id):
                ?>

    <div <?php echo 'id="section'.$page_id.'"' ; ?> >
        
				<?php 
                $page_data = get_page($page_id);
                echo apply_filters('the_content' , sh_set($page_data , 'post_content')) ; 
                ?>
		
	</div>

<?php  $count++;
endforeach; 

endif;?>
<?php get_footer(); ?>