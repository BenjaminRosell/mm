<?php /* Template Name: VC Page */
get_header() ;

$meta = _WSH()->get_meta();

?>
<?php while( have_posts() ): the_post(); ?>
     <?php the_content(); ?>
<?php endwhile;  ?>

<?php get_footer() ; ?>