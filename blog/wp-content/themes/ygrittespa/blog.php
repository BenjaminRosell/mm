<!-- Post -->
<div class="post-item">
    <!-- Post Title -->
    <h2 class="wow fadeInLeft"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
    <div class="post wow fadeInUp">
        <!-- Image -->
        <?php if ( has_post_thumbnail() ): ?>
        	<a href="<?php the_permalink();?>"><?php the_post_thumbnail('850x350', array('class' => 'img-responsive'));?></a>
        <?php endif;?>
        <div class="post-content wow fadeInUp">	
            <!-- Meta -->
            
            <!-- Text -->
            <p><?php echo get_the_excerpt();?></p>
            <div class="posted-date"><?php echo get_the_date('F d, Y');?>   <?php _e('  /  ', SH_NAME);?>   <span><?php _e('by', SH_NAME);?></span> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author();?></a>   <?php _e('   /   ', SH_NAME);?>   <a href="<?php the_permalink();?>#comments"><?php comments_number(); ?></a></div>
        </div>
    </div>
</div><!-- End Post -->	