<?php
$options = _WSH()->option();

get_header(); 

?>
<!--  Your Page Content Start From Here -->
<section id="page_title_area" class="page_title_area" >
    <!-- this is dummy heading for w3 validate approval you can remove it if you want -->
    <h2 class="none"><?php _e('For W3 validate', SH_NAME);?></h2>
    <!-- this is dummy heading for w3 validate approval you can remove it if you want -->
    
    <!-- container -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- title -->
                <h1 class="page_title"><?php _e('Not Found ', SH_NAME);?></h1>
                <!-- title -->
            </div>
        </div>
    </div>
    <!-- container -->
</section>
<!--  Your page Content End Here -->
<!--  Your Blog Content Start From Here -->
<section id="blog_area" class="blog_area">
    <!-- this is dummy heading for w3 validate approval you can remove it if you want -->
    <h2 class="none"><?php _e('For W3 validate', SH_NAME);?></h2>
    <!-- this is dummy heading for w3 validate approval you can remove it if you want -->
    
    <!-- container -->
    <div class="container">
        <div class="row">
            <!-- blog post area -->
            <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 blog_post_area">
            	<div class="message-welcome text-center">
                    <h1><?php _e('Not Found ', SH_NAME);?></h1>
                    <p class="lead"><?php _e('Look like something went wrong! The page you were looking for is not here. ', SH_NAME);?></p>
                    <a href="<?php echo home_url();?>" title="" class="btn btn-primary btn-lg"><?php _e('BACK TOP HOME', SH_NAME);?></a>
                </div><!-- end message -->
			</div>
            <!-- blog post area -->
		
        </div>
    </div>
    <!-- container -->
</section>
<!--  Your Blog Content End Here -->  		
<?php get_footer(); ?>