<?php ob_start(); ?>


<div class="stats">
    <div class="container">
        <ul class="list-inline" id="numbers">
                    <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s">
                        <span id="number1">0</span>
                        <h4><?php echo balanceTags($fact1bigword);?></h4>
                        <h5><?php echo balanceTags($fact1smallword);?></h5>
                    </li>
                    <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s">
                        <span id="number2">0</span>
                        <h4><?php echo balanceTags($fact2bigword);?></h4>
                        <h5><?php echo balanceTags($fact2smallword);?></h5>
                    </li>
                    <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s">
                        <span id="number3">0</span>
                        <h4><?php echo balanceTags($fact3bigword);?></h4>
                        <h5><?php echo balanceTags($fact3smallword);?></h5>
                    </li>
                    <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s">
                        <span id="number4">0</span>
                        <h4><?php echo balanceTags($fact4bigword);?></h4>
                        <h5><?php echo balanceTags($fact4smallword);?></h5>
                    </li>
                </ul>
    </div>
</div>
<script type="text/livescript">
////////////////////////////////////////////////////////
///////////////Animated Number ///////////////////////////
////////////////////////////////////////////////////////

    jQuery('#numbers').appear(function ($) {
        jQuery('#number1').animateNumber({number: 1258}, 1500);
        jQuery('#number2').animateNumber({number: 5856}, 1500);
        jQuery('#number3').animateNumber({number: 149}, 1500);
		jQuery('#number4').animateNumber({number: 6399}, 1500);
    }, {accX: 0, accY: -200});

</script>

<?php return ob_get_clean(); 