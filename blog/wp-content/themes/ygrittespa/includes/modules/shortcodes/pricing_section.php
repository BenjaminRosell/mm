<?php ob_start(); ?>

<div class="flat">
	<?php echo do_shortcode( $contents );?>
</div>

<?php return ob_get_clean(); ?>