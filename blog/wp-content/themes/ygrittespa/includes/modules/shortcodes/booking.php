<?php  
ob_start() ;?>

<div id="booking">
    <div class="container">
    <div class="row">
            <div class="col-md-12 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                <h3><?php echo balanceTags($title);?></h3>
                <p><i class="fa fa-phone"></i> <span><?php _e('Call Us at', SH_NAME);?></span><?php echo balanceTags($phone);?></p>
                <p><strong><?php _e('or', SH_NAME);?></strong></p>
                <p><i class="fa fa-envelope-o"></i> <span><?php _e('Send us an email:', SH_NAME);?></span><?php echo balanceTags($email);?></p>
            </div>
        </div>
    </div>
</div>        

<?php
	$output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>
   