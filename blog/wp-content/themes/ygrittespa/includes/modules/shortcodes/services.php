<?php  
   $count = 1;
   $query_args = array('post_type' => 'sh_services' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   
   if( $cat ) $query_args['services_category'] = $cat;
   $query = new WP_Query($query_args) ; 
   
   ob_start() ;?>
   
<?php if($query->have_posts()):  ?>   

<div class="services-grid row">
    
	<?php while($query->have_posts()): $query->the_post();
            global $post ; 
            $services_meta = _WSH()->get_meta();
	?>  
    
    <div class="col-md-6 col-sm-12 services-box wow <?php if(($count%2) == 0 && $count != 1) echo "left-padding-none fadeInRight"; else echo "right-padding-none fadeInLeft";?>" data-wow-duration="0.9s" data-wow-delay="0.9s">
        <div class="row">
            <div class="col-md-6 col-sm-6 right-padding-none">
                <div class="img-wrap">
                    <a href="<?php echo sh_set($services_meta, 'ext_url');?>"><?php the_post_thumbnail('300x420');?></a>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 left-padding-none">
                <div class="services-wrap">
                <div class="services-caption">
                    <h3><a href="<?php echo sh_set($services_meta, 'ext_url');?>"><?php the_title();?></a></h3>
                    <p><?php echo _sh_trim(get_the_excerpt(), $text_limit);?></p>
                    <a href="<?php echo sh_set($services_meta, 'ext_url');?>" class="btn main-btn hvr-sweep-to-right small-btn"><i class="fa fa-angle-right"></i><?php _e(' Learn more', SH_NAME);?></a>
                    
                </div>
                </div>
            </div>
        </div>
     </div>
     
     <?php $count++; endwhile; ?>
     
    
     
</div>

<?php endif; ?>

<?php 
	wp_reset_query();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>