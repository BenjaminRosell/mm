<?php  
   global $post ;
   $count = 0;
   $query_args = array('post_type' => 'post' , 'showposts' => $num , 'order_by' => $sort , 'order' => $order);
   if( $cat ) $query_args['category_name'] = $cat;
   $query = new WP_Query($query_args) ; 
   
   ob_start() ;?>
   
<?php if($query->have_posts()):  ?>   
<?php while($query->have_posts()): $query->the_post();
	global $post ; 
	$post_meta = _WSH()->get_meta();
?>
<div class="col-md-6">
<div class="post-item over">
        <!-- Post Title -->
        
        <div class="post wow fadeInUp">
         
            <!-- Image -->
            <?php the_post_thumbnail('850x350', array('class' => 'img-responsive hvr-grow'));?>
            
            <div class="post-content wow fadeInUp">	
                <!-- Meta -->
                <h2 class="wow fadeInLeft"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                <!-- Text -->
                <p><?php echo _sh_trim(get_the_excerpt(), $text_limit);?></p>
                <div class="posted-date"><?php echo get_the_date('F d, Y')?><?php _e('   /   ', SH_NAME);?><span><?php _e('by', SH_NAME);?></span> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author();?></a><?php _e('   /   ', SH_NAME);?><a href="<?php the_permalink();?>#comments"><?php comments_number();?></a></div>
            </div>
        </div>
    </div>
</div>
<?php endwhile;?>

<?php endif; ?>

<?php 
	wp_reset_query();
   $output = ob_get_contents(); 
   ob_end_clean(); 
   return $output ; ?>