<?php
$options = array();
$options[] = array(
	'id'          => '_sh_seo_settings',
	'types'       => array('post', 'page', 'product', 'sh_services', 'sh_team', 'sh_projects'),
	'title'       => __('SEO Settings', SH_NAME),
	'priority'    => 'low',
	'template'    => 
			array(
					
					array(
						'type' => 'toggle',
						'name' => 'seo_status',
						'label' => __('Enable SEO', SH_NAME),
						'description' => __('Enable / disable seo settings for this post', SH_NAME),
					),
					array(
						'type' => 'textbox',
						'name' => 'title',
						'label' => __('Meta Title', SH_NAME),
						'description' => __('Enter meta title or leave it empty to use default title', SH_NAME),
					),
					
					array(
						'type' => 'textarea',
						'name' => 'description',
						'label' => __('Meta Description', SH_NAME),
						'default' => '',
						'description' => __('Enter meta description', SH_NAME),
					),
					array(
						'type' => 'textarea',
						'name' => 'keywords',
						'label' => __('Meta Keywords', SH_NAME),
						'default' => '',
						'description' => __('Enter meta keywords', SH_NAME),
					),
				),
); /** SEO fields for custom posts and pages */
$options[] = array(
	'id'          => '_sh_layout_settings',
	'types'       => array('post', 'page', 'product', 'sh_services', ),
	'title'       => __('Layout Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 
			array(
					
					array(
						'type' => 'radioimage',
						'name' => 'layout',
						'label' => __('Page Layout', SH_NAME),
						'description' => __('Choose the layout for blog pages', SH_NAME),
						'items' => array(
							array(
								'value' => 'left',
								'label' => __('Left Sidebar', SH_NAME),
								'img' => SH_TH_URL.'/includes/vafpress/public/img/2cl.png',
							),
							array(
								'value' => 'right',
								'label' => __('Right Sidebar', SH_NAME),
								'img' => SH_TH_URL.'/includes/vafpress/public/img/2cr.png',
							),
							array(
								'value' => 'full',
								'label' => __('Full Width', SH_NAME),
								'img' => SH_TH_URL.'/includes/vafpress/public/img/1col.png',
							),
							
						),
					),
					
					array(
						'type' => 'select',
						'name' => 'sidebar',
						'label' => __('Sidebar', SH_NAME),
						'default' => '',
						'items' => sh_get_sidebars(true)	
					),
				),
);
$options[] = array(
	'id'          => '_sh_header_settings',
	'types'       => array('sh_portfolio', 'download'),
	'title'       => __('Header Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 
			array(
					
					array(
						'type' => 'upload',
						'name' => 'header_img',
						'label' => __('Header Background Image', SH_NAME),
						'description' => __('Choose the header background image', SH_NAME),
					),
					array(
						'type' => 'textbox',
						'name' => 'header_title',
						'label' => __('Header Title', SH_NAME),
						'description' => __('Enter header title', SH_NAME),
					),
					
				),
);
/*$options[] =  array(
	'id'          => _WSH()->set_meta_key('post'),
	'types'       => array('post'),
	'title'       => __('Post Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 
			array(		
					array(
					'type' => 'select',
					'name' => 'arrow_view',
					'label' => __('Arrow Layout for "Top Blog Posts" shortcode', SH_NAME),
					'default' => 'img_left',
					'items' => array(
									array(
										'value' => 'img_left',
										'label' => __('Image Left', SH_NAME),
									),
									array(
										'value' => 'img_right',
										'label' => __('Image Right', SH_NAME),
									),
									array(
										'value' => 'img_top',
										'label' => __('Image Top', SH_NAME),
									),
								),
					),
					array(
						'type' => 'textarea',
						'name' => 'description',
						'label' => __('Post Description', SH_NAME),
						'default' => '',
						'description' => __('Enter the post description for detail page.', SH_NAME)
					),
					array(
						'type' => 'textarea',
						'name' => 'video',
						'label' => __('Video Embed Code', SH_NAME),
						'default' => '',
						'description' => __('If post format is video then this embed code will be used in content', SH_NAME)
					),
					array(
						'type' => 'textarea',
						'name' => 'audio',
						'label' => __('Audio Embed Code', SH_NAME),
						'default' => '',
						'description' => __('If post format is AUDIO then this embed code will be used in content', SH_NAME)
					),
					array(
						'type' => 'textarea',
						'name' => 'quote',
						'label' => __('Quote', SH_NAME),
						'default' => '',
						'description' => __('If post format is quote then the content in this textarea will be displayed', SH_NAME)
					),
							
					
			),
);*/
/* Page options */
/** Team Options*/
$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_slide'),
	'types'       => array('sh_slide'),
	'title'       => __('Slides Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
	
				array(
					'type'      => 'group',
					'repeating' => true,
					'length'    => 1,
					'name'      => 'sh_slide_text',
					'title'     => __('Slide Content', SH_NAME),
					'fields'    => array(
						
						array(
							'type' => 'textarea',
							'name' => 'slide_text',
							'label' => __('Slide Text', SH_NAME),
							'default' => '',
							
						),
						
						
					),
				),
	),
);
/** Services Options*/
$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_services'),
	'types'       => array( 'sh_services' ),
	'title'       => __('Services Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => 
			array(
				
				array(
					'type' => 'fontawesome',
					'name' => 'fontawesome',
					'label' => __('Service Icon', SH_NAME),
					'default' => '',
				),
				
				array(
					'type' => 'textbox',
					'name' => 'ext_url',
					'label' => __('Read more link', SH_NAME),
					'default' => '',
				),
				
			),
);
/** Gallery Options
$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_gallery'),
	'types'       => array('sh_gallery'),
	'title'       => __('Image Gallery Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
			array(
				'type' => 'textbox',
				'name' => 'ext_url',
				'label' => __('Read more link', SH_NAME),
				'default' => '',
			    ),
	),
);*/
/** Team Options*/
$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_team'),
	'types'       => array('sh_team'),
	'title'       => __('Team Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
	
						
				array(
					'type' => 'textbox',
					'name' => 'designation',
					'label' => __('Designation', SH_NAME),
					'default' => '',
				),
				array(
					'type' => 'textbox',
					'name' => 'team_link',
					'label' => __('Team Link', SH_NAME),
					'default' => '#',
				),
				array(
					'type'      => 'group',
					'repeating' => true,
					'length'    => 1,
					'name'      => 'sh_team_social',
					'title'     => __('Social Profile', SH_NAME),
					'fields'    => array(
						
						array(
							'type' => 'fontawesome',
							'name' => 'social_icon',
							'label' => __('Social Icon', SH_NAME),
							'default' => '',
						),
						
						array(
							'type' => 'textbox',
							'name' => 'social_link',
							'label' => __('Link', SH_NAME),
							'default' => '',
							
						),
						
						
					),
				),
	),
);
/** Testimonial Options*/
$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_testimonials'),
	'types'       => array('sh_testimonials'),
	'title'       => __('Testimonials Options', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
				array(
					'type' => 'textbox',
					'name' => 'designation',
					'label' => __('Designation', SH_NAME),
					'default' => 'Consultant',
				),
				array(
					'type' => 'textbox',
					'name' => 'ext_url',
					'label' => __('External Link', SH_NAME),
					'default' => '#',
				)
	),
);
/** Projects Options
$options[] =  array(
	'id'          => _WSH()->set_meta_key('sh_projects'),
	'types'       => array('sh_projects'),
	'title'       => __('Image Gallery Settings', SH_NAME),
	'priority'    => 'high',
	'template'    => array(
					
					array(
						'type' => 'textbox',
						'name' => 'ext_url',
						'label' => __('Read more link', SH_NAME),
						'default' => '',
					),
						
									
	),
);*/
/**
 * EOF
 */
 
 
 return $options; 