<?php

if( !function_exists('_WSH') ) {

	function _WSH()

	{

		return $GLOBALS['_sh_base'];

	}

}



function sh_font_awesome( $code = false )

{

	$pattern = '/\.(fa-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';

	$subject = @file_get_contents(SH_TH_ROOT.'/includes/vafpress/public/css/vendor/font-awesome.css');



	if( !$subject ) return array();

	

	preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);



	$icons = array();

	

	$icons[0] = __('No Icon', SH_NAME);

	

	

	foreach($matches as $match){

		$value = str_replace( 'icon-', '', $match[1] );

		$newcode = $match[1];//str_replace('fa-', '', $match[1]);

		

		if( $code ) $icons[$match[1]] = stripslashes($match[2]);

		else $icons[$newcode] = ucwords(str_replace('fa-', ' ', $newcode));//$match[2];

	}

	

	return $icons;

}



if( !function_exists( 'sh_theme_color_scheme' ) )

{

	function sh_theme_color_scheme()

	{	

		$options = _WSH()->option();

		$dir = SH_TH_ROOT;

		include_once($dir.'/includes/thirdparty/lessc.inc.php');

		$styles = _WSH()->option('custom_color_scheme');

		

		if( ! $styles ) return;	

		

		$transient = get_transient( '_sh_color_scheme' );

		

	

		$update = ( $styles != $transient ) ? true : false;

		if(sh_set($options, 'color_scheme') == 'custom') wp_enqueue_style( 'custom_colors', _WSH()->includes( 'assets/css/colors.css', true ) );

		//echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/css/colors.css" />';

		

		//if( !$update ) return;

	

		set_transient( '_sh_color_scheme', $styles, DAY_IN_SECONDS );

		

		$less = new lessc;

	

		$less->setVariables(array(

		  "sh_color" => $styles,

		));

		

		// create a new cache object, and compile

		$cache = $less->cachedCompile( _WSH()->includes("/assets/css/color.less" ) );

	

		file_put_contents( _WSH()->includes('/assets/css/colors.css'), $cache["compiled"]);

		

	}

}





