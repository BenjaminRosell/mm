<?php
$sh_sc = array();
$sh_sc['sh_heading']	=	array(
					"name" => __("Section Header", SH_NAME),
					"base" => "sh_heading",
					"class" => "",
					"category" => __('Ygritte Spa', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Section Header.', SH_NAME),
					"params" => array(
					   	
					 	array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Section id', SH_NAME ),
						   "param_name" => "id",
						   "description" => __('Enter section id for anchor', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of header', SH_NAME )
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Description Text', SH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter Description Text for header', SH_NAME )
						),
					)
				);

$sh_sc['sh_about']	=	array(
					"name" => __("About Section", SH_NAME),
					"base" => "sh_about",
					"class" => "",
					"category" => __('Ygritte Spa', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Section About.', SH_NAME),
					"params" => array(
					   	
					 	array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of section', SH_NAME )
						),
						array(
						   "type" => "textarea_html",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('About Text', SH_NAME ),
						   "param_name" => "text",
						   "description" => __('Enter About Text for Section', SH_NAME )
						),
						array(
						   "type" => "textarea",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('About features', SH_NAME ),
						   "param_name" => "feature_str",
						   "description" => __('Enter About feature list', SH_NAME )
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Image 1", SH_NAME),
						   "param_name" => "img1",
						   'value' => '',
						   "description" => __("Enter the Image 1 url", SH_NAME)
						),
						array(
						   "type" => "attach_image",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Image 2", SH_NAME),
						   "param_name" => "img2",
						   'value' => '',
						   "description" => __("Enter the Image 2 url", SH_NAME)
						),
					)
				);
$sh_sc['sh_team']	=	array(
					"name" => __("Team", SH_NAME),
					"base" => "sh_team",
					"class" => "",
					"category" => __('Ygritte Spa', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Team.', SH_NAME),
					"params" => array(
					   	
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of members to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter text limit for excerpt.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'team_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);
$sh_sc['sh_services']	=	array(
					"name" => __("Services", SH_NAME),
					"base" => "sh_services",
					"class" => "",
					"category" => __('Ygritte Spa', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show services.', SH_NAME),
					"params" => array(
					   	
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of Slides to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter the text limit to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'services_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);
$sh_sc['sh_pricing_section']	=	array(
			"name" => __("Pricing Section", SH_NAME),
			"base" => "sh_pricing_section",
			"class" => "",
			"category" => __('Ygritte Spa', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			"as_parent" => array('only' => 'sh_pricing_table'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
			"content_element" => true,
			"show_settings_on_create" => false,
			'description' => __('Add Number of pricing tables to your theme.', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Description", SH_NAME),
				   "param_name" => "description",
				   "description" => __("Enter the description for help.", SH_NAME)
				),
			),
			"js_view" => 'VcColumnView'
		);
$sh_sc['sh_pricing_table']	=	array(
			"name" => __("Pricing Table", SH_NAME),
			"base" => "sh_pricing_table",
			"class" => "",
			"category" => __('Ygritte Spa', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			"as_child" => array('only' => 'sh_pricing_section'),// Use only|except attributes to limit child shortcodes (separate multiple values with comma)
			"content_element" => true,
			"show_settings_on_create" => true,
			'description' => __('Add Pricing Table.', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the title to show.", SH_NAME)
				),
				array(
				   "type" => "textarea",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Features", SH_NAME),
				   "param_name" => "feature_str",
				   "description" => __("Enter the features to show one per line and strong text is seperated by '||'", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Price", SH_NAME),
				   "param_name" => "price",
				   "description" => __("Enter the Price to show", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Package Duration", SH_NAME),
				   "param_name" => "package_duration",
				   "description" => __("Enter the package duration to show", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Text", SH_NAME),
				   "param_name" => "btn_text",
				   "description" => __("Enter the button text to show", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Button Link", SH_NAME),
				   "param_name" => "btn_link",
				   "description" => __("Enter the button link to show", SH_NAME)
				),
			
			),
		);
$sh_sc['sh_portfolio'] = array(
			"name" => __("Show Portfolio", SH_NAME),
			"base" => "sh_portfolio",
			"class" => "",
			"category" => __('Ygritte Spa', SH_NAME),
			"icon" => 'fa-user' ,
			'description' => __('show portfolio 4 column', SH_NAME),
			"params" => array(
				 	
					array(
					   "type" => "textfield",
					   "holder" => "div",
					   "class" => "",
					   "heading" => __('Number', SH_NAME ),
					   "param_name" => "num",
					   "description" => __('Enter Number of portfolios to Show.', SH_NAME )
					),
					array(
					   "type" => "dropdown",
					   "holder" => "div",
					   "class" => "",
					   "heading" => __( 'Category', SH_NAME ),
					   "param_name" => "cat",
					   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'projects_category', 'hide_empty' => FALSE ), true ) ),
					   "description" => __( 'Choose Category.', SH_NAME )
					),
					array(
					   "type" => "dropdown",
					   "holder" => "div",
					   "class" => "",
					   "heading" => __("Order By", SH_NAME),
					   "param_name" => "sort",
					   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
					   "description" => __("Enter the sorting order.", SH_NAME)
					),
					array(
					   "type" => "dropdown",
					   "holder" => "div",
					   "class" => "",
					   "heading" => __("Order", SH_NAME),
					   "param_name" => "order",
					   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
					   "description" => __("Enter the sorting order.", SH_NAME)
					),
					
			)
	    );

$sh_sc['sh_booking']	=	array(
					"name" => __("Booking Section", SH_NAME),
					"base" => "sh_booking",
					"class" => "",
					"category" => __('Ygritte Spa', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Section booking.', SH_NAME),
					"params" => array(
					   	
					 	array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Title', SH_NAME ),
						   "param_name" => "title",
						   "description" => __('Enter Title of section', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Phone Number', SH_NAME ),
						   "param_name" => "phone",
						   "description" => __('Enter Phone Number of section', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Email', SH_NAME ),
						   "param_name" => "email",
						   "description" => __('Enter email of section', SH_NAME )
						),
						
					)
				);

$sh_sc['sh_testimonial']	=	array(
					"name" => __("Testimonials", SH_NAME),
					"base" => "sh_testimonial",
					"class" => "",
					"category" => __('Ygritte Spa', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show testimonials.', SH_NAME),
					"params" => array(
					
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of testimonials to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter the text limit to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'testimonials_category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
			
					)
				);
$sh_sc['sh_blog']	=	array(
					"name" => __("Blog Posts", SH_NAME),
					"base" => "sh_blog",
					"class" => "",
					"category" => __('Ygritte Spa', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Blog Posts.', SH_NAME),
					"params" => array(
					   	
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Number', SH_NAME ),
						   "param_name" => "num",
						   "description" => __('Enter Number of posts to Show.', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Text Limit', SH_NAME ),
						   "param_name" => "text_limit",
						   "description" => __('Enter text limit for posts to Show.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __( 'Category', SH_NAME ),
						   "param_name" => "cat",
						   "value" => array_flip( (array)sh_get_categories( array( 'taxonomy' => 'category', 'hide_empty' => FALSE ), true ) ),
						   "description" => __( 'Choose Category.', SH_NAME )
						),
						array(
						   "type" => "dropdown",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order By", SH_NAME),
						   "param_name" => "sort",
						   'value' => array_flip( array('date'=>__('Date', SH_NAME),'title'=>__('Title', SH_NAME) ,'name'=>__('Name', SH_NAME) ,'author'=>__('Author', SH_NAME),'comment_count' =>__('Comment Count', SH_NAME),'random' =>__('Random', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
						array(
						   "type" => "dropdown",
						   
						   "holder" => "div",
						   "class" => "",
						   "heading" => __("Order", SH_NAME),
						   "param_name" => "order",
						   'value' => array_flip(array('ASC'=>__('Ascending', SH_NAME),'DESC'=>__('Descending', SH_NAME) ) ),			
						   "description" => __("Enter the sorting order.", SH_NAME)
						),
					)
				);
$sh_sc['sh_contact']	=	array(
					"name" => __("Contact Form", SH_NAME),
					"base" => "sh_contact",
					"class" => "",
					"category" => __('Ygritte Spa', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Contact info.', SH_NAME),
					"params" => array(
					   	array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Description', SH_NAME ),
						   "param_name" => "description",
						   "description" => __('Enter the description for help', SH_NAME )
						),
						
					)
				);

$sh_sc['sh_location_facts']	=	array(
			"name" => __("Location Facts", SH_NAME),
			"base" => "sh_location_facts",
			"class" => "",
			"category" => __('Ygritte Spa', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			"as_parent" => array('only' => 'sh_location_fact'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
			"content_element" => true,
			"show_settings_on_create" => false,
			'description' => __('Add Location Facts to your theme.', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Description", SH_NAME),
				   "param_name" => "description",
				   "description" => __("Enter the Description for help.", SH_NAME)
				),
			
			),
			"js_view" => 'VcColumnView'
		);
$sh_sc['sh_location_fact']	=	array(
			"name" => __("Location Fact", SH_NAME),
			"base" => "sh_location_fact",
			"class" => "",
			"category" => __('Ygritte Spa', SH_NAME),
			"icon" => 'icon-wpb-layer-shape-text' ,
			"as_child" => array('only' => 'sh_location_facts'),// Use only|except attributes to limit child shortcodes (separate multiple values with comma)
			"content_element" => true,
			"show_settings_on_create" => true,
			'description' => __('Add Location Fact.', SH_NAME),
			"params" => array(
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Title", SH_NAME),
				   "param_name" => "title",
				   "description" => __("Enter the Title to show.", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Address", SH_NAME),
				   "param_name" => "address",
				   "description" => __("Enter the address to show.", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Phone", SH_NAME),
				   "param_name" => "phone",
				   "description" => __("Enter the Phone number to show.", SH_NAME)
				),
				array(
				   "type" => "textfield",
				   "holder" => "div",
				   "class" => "",
				   "heading" => __("Email", SH_NAME),
				   "param_name" => "email",
				   "description" => __("Enter the Email to show.", SH_NAME)
				),
							
			),
		);
$sh_sc['sh_map']	=	array(
					"name" => __("Google map", SH_NAME),
					"base" => "sh_map",
					"class" => "",
					"category" => __('Ygritte Spa', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Google map.', SH_NAME),
					"params" => array(
					   	
					 	array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Lattitude', SH_NAME ),
						   "param_name" => "lat",
						   "description" => __('Enter Lattitude for map', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Longitude', SH_NAME ),
						   "param_name" => "long",
						   "description" => __('Enter Longitude for map', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Mark Lattitude', SH_NAME ),
						   "param_name" => "mark_lat",
						   "description" => __('Enter Mark Lattitude for map', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Mark Longitude', SH_NAME ),
						   "param_name" => "mark_long",
						   "description" => __('Enter Mark Longitude for map', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Mark Title', SH_NAME ),
						   "param_name" => "mark_title",
						   "description" => __('Enter Mark Title for map', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Mark Address', SH_NAME ),
						   "param_name" => "mark_address",
						   "description" => __('Enter Mark Address for map', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Mark phone', SH_NAME ),
						   "param_name" => "mark_phone",
						   "description" => __('Enter Mark phone for map', SH_NAME )
						),
						
					)
				);
$sh_sc['sh_statics']	=	array(
					"name" => __("Section Statics", SH_NAME),
					"base" => "sh_statics",
					"class" => "",
					"category" => __('Ygritte Spa', SH_NAME),
					"icon" => 'fa-briefcase' ,
					'description' => __('Show Section Header.', SH_NAME),
					"params" => array(
					   	
					 	array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 1 big word', SH_NAME ),
						   "param_name" => "fact1bigword",
						   "description" => __('Enter Fact 1 big word', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 1 small word', SH_NAME ),
						   "param_name" => "fact1smallword",
						   "description" => __('Enter Fact 1 small word', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 1 number', SH_NAME ),
						   "param_name" => "fact1num",
						   "description" => __('Enter Fact 1 number', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 2 big word', SH_NAME ),
						   "param_name" => "fact2bigword",
						   "description" => __('Enter Fact 2 big word', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 2 small word', SH_NAME ),
						   "param_name" => "fact2smallword",
						   "description" => __('Enter Fact 2 small word', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 2 number', SH_NAME ),
						   "param_name" => "fact2num",
						   "description" => __('Enter Fact 2 number', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 3 big word', SH_NAME ),
						   "param_name" => "fact3bigword",
						   "description" => __('Enter Fact 3 big word', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 3 small word', SH_NAME ),
						   "param_name" => "fact3smallword",
						   "description" => __('Enter Fact 3 small word', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 3 number', SH_NAME ),
						   "param_name" => "fact3num",
						   "description" => __('Enter Fact 3 number', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 4 big word', SH_NAME ),
						   "param_name" => "fact4bigword",
						   "description" => __('Enter Fact 4 big word', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 4 small word', SH_NAME ),
						   "param_name" => "fact4smallword",
						   "description" => __('Enter Fact 4 small word', SH_NAME )
						),
						array(
						   "type" => "textfield",
						   "holder" => "div",
						   "class" => "",
						   "heading" => __('Fact 4 number', SH_NAME ),
						   "param_name" => "fact4num",
						   "description" => __('Enter Fact 4 number', SH_NAME )
						),
					)
				);
				
/*----------------------------------------------------------------------------*/
$sh_sc = apply_filters( '_sh_shortcodes_array', $sh_sc );
	
return $sh_sc;