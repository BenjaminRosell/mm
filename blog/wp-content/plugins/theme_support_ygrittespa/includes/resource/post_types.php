<?php

$theme_option = _WSH()->option() ; 

$slide_slug = sh_set($theme_option , 'slide_permalink' , 'slides') ;

$service_slug = sh_set($theme_option , 'service_permalink' , 'services') ;

$projects_slug = sh_set($theme_option , 'gallery_permalink' , 'gallery') ;

$team_slug = sh_set($theme_option , 'team_permalink' , 'teams') ;

$testimonial_slug = sh_set($theme_option , 'testimonial_permalink' , 'testimonials') ;



$options = array();



/*$options['sh_slide'] = array(

								'labels' => array(__('Slide', SH_NAME), __('Slides', SH_NAME)),

								'slug' => $slide_slug ,

								'label_args' => array('menu_name' => __('Slider', SH_NAME)),

								'supports' => array( 'title', 'thumbnail'),

								'label' => __('Slider', SH_NAME),

								'args'=>array(

											'menu_icon'=>'dashicons-slides' , 

											'taxonomies'=>array('slide_category')

								)

							);*/



$options['sh_services'] = array(

								'labels' => array(__('Service', SH_NAME), __('Service', SH_NAME)),

								'slug' => $service_slug ,

								'label_args' => array('menu_name' => __('Services', SH_NAME)),

								'supports' => array( 'title' , 'editor' , 'thumbnail' ),

								'label' => __('Service', SH_NAME),

								'args'=>array(

										'menu_icon'=>'dashicons-products' , 

										'taxonomies'=>array('services_category')

								)

							);

$options['sh_projects'] = array(

								'labels' => array(__('Portfolio', SH_NAME), __('Portfolio', SH_NAME)),

								'slug' => $projects_slug ,

								'label_args' => array('menu_name' => __('Portfolio', SH_NAME)),

								'supports' => array( 'title' , 'editor' , 'thumbnail', 'comments'),

								'label' => __('Portfolio', SH_NAME),

								'args'=>array(

											'menu_icon'=>'dashicons-format-gallery' , 

											'taxonomies'=>array('projects_category')

								)

							);							

							

$options['sh_team'] = array(

								'labels' => array(__('Member', SH_NAME), __('Member', SH_NAME)),

								'slug' => $team_slug ,

								'label_args' => array('menu_name' => __('Teams', SH_NAME)),

								'supports' => array( 'title', 'editor' , 'thumbnail'),

								'label' => __('Member', SH_NAME),

								'args'=>array(

											'menu_icon'=>'dashicons-groups' , 

											'taxonomies'=>array('team_category')

								)

							);



$options['sh_testimonials'] = array(

								'labels' => array(__('Testimonial', SH_NAME), __('Testimonial', SH_NAME)),

								'slug' => $testimonial_slug ,

								'label_args' => array('menu_name' => __('Testimonials', SH_NAME)),

								'supports' => array( 'title' , 'editor' , 'thumbnail' ),

								'label' => __('Testimonial', SH_NAME),

								'args'=>array(

										'menu_icon'=>'dashicons-testimonial' , 

										'taxonomies'=>array('testimonials_category')

								)

							);

